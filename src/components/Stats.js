import React, { useEffect, useState } from 'react';
import { getStats } from '../api';
import './scss/Stats.scss';

function Stats({ colors }) {
  const [stats, setStats] = useState([]);

  let statResult = [];

  if (colors && stats) {
    stats.forEach((data, index) => {
      statResult[index] = { ...data, color: colors[data.result] };
    });
  }

  useEffect(() => {
    getStats().then(data => {
      setStats(data);
    });
  }, []);

  return (
    <div className='stat-wrapper'>
      <table className='table'>
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <th colSpan='5' className='cold'>
              Cold
            </th>
            <th colSpan='27' className='neutral'>
              Neutral
            </th>
            <th colSpan='5' className='hot'>
              Hot
            </th>
          </tr>
          <tr>
            <th>Slot</th>
            {statResult &&
              statResult.map((data, i) => {
                return (
                  <td key={i} className={`stat-list-item ${data.color}`}>
                    {data.result}
                  </td>
                );
              })}
          </tr>
          <tr className='hits'>
            <th>Hits</th>
            {statResult &&
              statResult.map((data, i) => {
                return (
                  <td
                    className={`stat-list-item ${i < 5 ? 'cold' : ''} ${
                      i > 31 ? 'hot' : ''
                    }`}
                    key={i}
                  >
                    {data.count}
                  </td>
                );
              })}
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Stats;
