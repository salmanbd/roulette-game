import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { getHistory, getNextGame } from '../api';
import './scss/History.scss';

function History() {
  const [history, setHistory] = useState([]);
  const [nextGame, setNextGame] = useState({});

  // For countdown state
  const then = moment.duration('nextGame.startTime');
  const now = moment();
  const countdown = moment(then - now);
  const startCount = countdown.format('ss');
  const [counter, setCounter] = useState(startCount);

  useEffect(() => {
    const timer =
      counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  useEffect(() => {
    getNextGame().then(data => {
      setNextGame(data);
    });
  }, [Number(counter) === 0]);

  useEffect(() => {
    getHistory().then(data => {
      setHistory(data);
    });
  }, []);

  return (
    <ul className='history-list'>
      {history &&
        history.map(data => {
          return (
            <li
              key={data.uuid}
            >{`Game ${data.id} ended, result is ${data.result}`}</li>
          );
        })}
      <li>{`Game ${nextGame.id} will start in ${counter} sec`}</li>
    </ul>
  );
}

export default History;
