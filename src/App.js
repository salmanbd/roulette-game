import React, { useEffect, useState } from 'react';
import { getConfiguration } from './api';
import { Stats, History } from './components';
import './App.scss';

function App() {
  const [configuration, setConfiguration] = useState({});

  const { colors, positionToId } = configuration;
  let gameResult = [];
  if (positionToId) {
    positionToId.forEach((data, index) => {
      gameResult[index] = { value: data, color: colors[data] };
    });
  }

  useEffect(() => {
    getConfiguration().then(data => {
      setConfiguration(data);
    });
  }, []);

  return (
    <div className='App'>
      <div className='container'>
        <h1>Aardvark Roulette game</h1>
        <h3>Stats (last 200)</h3>
        <Stats colors={colors} />
        <h3>Game Board</h3>
        <div className='game-board-btns'>
          {gameResult &&
            gameResult.map((data, i) => {
              return (
                <span key={i}>
                  <button className={`btn ${data.color}`}>{data.value}</button>
                  {i === 11 && <br></br>}
                  {i === 23 && <br></br>}
                  {i === 35 && <br></br>}
                </span>
              );
            })}
        </div>
        <h3>Events</h3>
        <History />
      </div>
    </div>
  );
}

export default App;
