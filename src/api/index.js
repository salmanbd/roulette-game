import axios from 'axios';
import baseUrl from '../settings';

export async function getConfiguration() {
  return axios
    .get(`${baseUrl.v1}/1/configuration`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export async function getNextGame() {
  return axios
    .get(`${baseUrl.v1}/1/nextGame`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export async function getHistory() {
  return axios
    .get(`${baseUrl.v1}/1/history`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export async function getScheduledGames() {
  return axios
    .get(`${baseUrl.v1}/1/scheduledGames`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}

export async function getStats() {
  return axios
    .get(`${baseUrl.v1}/1/stats?limit=200`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}
